﻿// Copyright (C) 2013-2014 Thalmic Labs Inc.
// Distributed under the Myo SDK license agreement. See LICENSE.txt for details.
#define _USE_MATH_DEFINES
#include <cmath>
#include <math.h>
#include <numeric>
#include <functional>
#include <iostream>
#include <iomanip>
#include <stdexcept>
#include <string>
#include <algorithm>
#include <fstream>
#include <unordered_map>
#include <windows.h>
#include "Shlwapi.h"
#include <direct.h>
#include <array>
#include <sstream>
#include <thread>
#include <bitset>
#include "src\include\floatfann.h"
#include "src\include\fann_cpp.h"
//#include "src\include\fixedfann.h"

// The only file that needs to be included to use the Myo C++ SDK is myo.hpp.
#include <myo/myo.hpp>
#include "TemplateMatcher.h"


// Classes that inherit from myo::DeviceListener can be used to receive events from Myo devices. DeviceListener
// provides several virtual functions for handling different kinds of events. If you do not override an event, the
// default behavior is to do nothing.
class DataCollector : public myo::DeviceListener {
public:
	std::unordered_map<int, std::string> poseMap;

	const float learning_rate = 0.7f;
	const unsigned int num_layers = 3;
	const unsigned int num_input = 40;
	const unsigned int num_hidden = 160;
	const unsigned int num_output = 8;
	const float desired_error = 0.005f;
	const unsigned int max_intervals = 1000*5*14;
	const unsigned int iterations_between_reports = 1000;
	unsigned int iterations = 0;
	FANN::neural_net net{ 1.0f, num_layers, num_input, num_hidden ,num_output };
	FANN::training_data data;
	fann_type extractedFeatures[40];
	bool test_done = true;
	std::vector<std::string>  inputVector;

	int announceCounter = 0;
	int timer = 5;
	const std::string announcements[14] = {"Fist", "Rest", "Fingerspread", "Rest", "WaveOut", "Rest", "WaveIn", "Rest", "Point Finger", "Rest", "MiddleFinger","Rest","Phone gesture","Rest" };

	// These values are set by onArmSync() and onArmUnsync() above.
	bool onArm;
	myo::Arm whichArm;

	// This is set by onUnlocked() and onLocked() above.
	bool isUnlocked;

	// These values are set by onOrientationData() and onPose() above.
	int roll_w, pitch_w, yaw_w;
	std::vector<std::vector<int>> sensorData{ 8 };
	std::vector<float> mavOld = std::vector<float>( 8,0.0f );
	std::array<int8_t, 8> emgSamples;

    DataCollector()
    : onArm(false), isUnlocked(false), roll_w(0), pitch_w(0), yaw_w(0)
    {

		for (int i = 0; i < num_output; i++) {
			sensorData[i] = std::vector<int>();
		}

		poseMap[5] = "DreiPunkteGriff";
		poseMap[1] = "Korbgriff";
		poseMap[4] = "DaumenFlexion";
		poseMap[3] = "Lateralgriff";
		poseMap[2] = "HandÖffnen";
		poseMap[0] = "Ruhe";
		
		if (fileExists("c:\\1\\Speech Test 1\\reco\\network")) {
			net = FANN::neural_net("c:\\1\\Speech Test 1\\reco\\network");
			iterations = max_intervals;
			test_done = true;
		}
		else {
			iterations = max_intervals;
			net.set_learning_rate(learning_rate);
			net.set_training_algorithm(FANN::TRAIN_QUICKPROP);

			net.set_activation_steepness_hidden(1.0);
			net.set_activation_steepness_output(1.0);

			net.set_activation_function_hidden(FANN::SIGMOID);
			net.set_activation_function_output(FANN::SIGMOID);

			std::ostringstream oss;
			oss << 399 << " " << num_input << " " << num_output << "\n";
			std::string input = oss.str();
			//writeFile("training.data", input);
			std::cout << "Current gesture is Rest" << std::endl;
			std::cout << "Hold for 5 seconds" << std::endl;
			std::cout << "Next gesture is " << announcements[announceCounter++] << " in " << timer << std::endl;
		}
    }

    // onUnpair() is called whenever the Myo is disconnected from Myo Connect by the user.
    void onUnpair(myo::Myo* myo, uint64_t timestamp)
    {
        // We've lost a Myo.
        // Let's clean up some leftover state.
        roll_w = 0;
        pitch_w = 0;
        yaw_w = 0;
        onArm = false;
		isUnlocked = false;
    }

	void announce() {
		if (iterations % 1000 == 0 && iterations != 0) {
			std::cout << timer << std::endl;
			timer--;
		}
		if (iterations % 5000 == 0 && iterations != 0) {
			std::cout << "Hold for 5 seconds" << std::endl;
			timer = 5;
			std::cout << "Next gesture is " << announcements[announceCounter++] << " in " << timer-- << std::endl;
			
		}
		
	}

	// onEmgData() is called whenever a paired Myo has provided new EMG data, and EMG streaming is enabled.
	void onEmgData(myo::Myo* myo, uint64_t timestamp, const int8_t* emg)
	{
		for (int i = 0; i < num_output; i++) {
			emgSamples[i] = emg[i];
			sensorData[i].push_back(static_cast<int>(emgSamples[i]));
		}
		if (iterations < max_intervals) {
			announce();
			if (iterations % 100 == 0 && iterations != 0 && (announcements[announceCounter]!="Rest" || announceCounter==1)) {
				computeSensorData();
				writeInputToVector();
				for (int i = 0; i < sensorData.size();i++) {
					sensorData[i].clear();
				}	
			}
			iterations++;
		}
		else {
			if (test_done) {
				writeVectorToFile();
				if (data.read_train_from_file("c:\\1\\Speech Test 1\\reco\\training.data"))
				{
					net.init_weights(data);
					net.train_on_data(data, max_intervals,
						iterations_between_reports, desired_error);
					std::cout <<  "\n" << "Testing network." <<  "\n";
				}
				test_done = false;
				std::cout << net.test_data(data) << "\n";
				net.save("c:\\1\\Speech Test 1\\reco\\network");
			}
			if (iterations % 100 == 0) {
				std::array<fann_type, 8> calc_out = ceilAll(softmax(net.run(computeSensorData())));
				for (int i = num_output - 1; i >= 0; i--) {
					std::cout << calc_out[i] << " ";
				}
				auto max = std::max_element(calc_out.begin(), calc_out.end());
				//writeFile("recognition.pipe", poseMap[std::distance(calc_out.begin(), max) + 1]);
				//writeFile("recognition.txt", poseMap[std::distance(calc_out.begin(), max) + 1]);

				if (!fileExists("c:\\1\\Speech Test 1\\reco\\recognition.pipe") && (std::distance(calc_out.begin(), max)) + 1 <=5) {
					auto search = poseMap[std::distance(calc_out.begin(), max) + 1];
						writeFile("recognition.txt", search);
						writeFile("recognition.pipe", "");
					
				}

				std::cout <<  "\n";
				for (int i = 0; i < sensorData.size(); i++) {
					sensorData[i].clear();
				}
			}
			iterations++;
		}
	}

	std::array<fann_type, 8> ceilAll(std::array<fann_type, 8> output) {
		std::array<fann_type, 8> softmax;
		float max = 0;
		float maxIndex = 0;
		for (int i = 0; i < num_output; i++) {
			if (output[i] > max) {
				max = output[i];
				maxIndex = i;
			}
		}
		for (int i = 0; i < num_output; i++) {
			if (i == maxIndex) {
				softmax[i] = 1;
			}
			else {
				softmax[i] = 0;
			}
		}
		return softmax;
	}

	std::array<fann_type, 8> softmax(fann_type* output) {
		std::array<fann_type, 8> softmax;
		float sum = 0;
		for (int i = 0; i < num_output; i++) {
			sum += output[i];
		}
		for (int i = 0; i < num_output; i++) {
			softmax[i] = output[i]/sum;
		}
		return softmax;
	}

	int sgn(int val) {
		return (0 < val) - (val < 0);
	}

	//TODO: Calculate all features in ONE iteration of the array, so O(n)
	int countSignChange(std::vector<int> sensor) {
		int count = 0;
		for (int i = 1; i < sensor.size(); i++) {
			if ((sgn(sensor[i - 1]) == -1 && sgn(sensor[i]) == 1 || sgn(sensor[i - 1]) == 1 && sgn(sensor[i]) == -1) && (abs(sensor[i-1] - sensor[i]) > 10))
				count++;
		}
		return count;
	}

	int countZeroPassing(std::vector<int> sensor) {
		int count = 0;
		for (int i = 1; i < sensor.size()-1; i++) {
			if ((sensor[i - 1] < sensor[i] > sensor[i+1] && abs(sensor[i] - sensor[i+1]) > 10) || (sensor[i - 1] > sensor[i] < sensor[i + 1] && abs(sensor[i] - sensor[i-1]) > 10))
				count++;
		}

		return count;
	}

	fann_type* computeSensorData() {
		int i = 0;
		for (auto sensor : sensorData) {
			extractedFeatures[i] = (float)std::accumulate(sensor.begin(), sensor.end(), int(0), [](int x, int y) { return abs(x) + abs(y); })/ (float)sensor.size();
			extractedFeatures[i + 1] = (float)(extractedFeatures[i] - mavOld[i / 5]);
			mavOld[i / 5] = extractedFeatures[i];
			extractedFeatures[i + 2] = (float) countSignChange(sensor);
			extractedFeatures[i + 3] = (float) countZeroPassing(sensor);
			extractedFeatures[i + 4] = (float) std::sqrt(std::accumulate(sensor.begin(), sensor.end(), int(0), [](int x, int y) { return abs(x) + pow(abs(y), 2); }) / (float) sensor.size());
			i+=5;
		}
		return extractedFeatures;
	}

	void writeInputToFile() {
		std::ostringstream oss;
		for (size_t i = 0; i < num_input; i++) {
			oss << static_cast<float>(extractedFeatures[i]) << " ";
		}
		oss <<  "\n";
		std::array<int, 8> output = encodeToOneInC(iterations/5000);
		for (size_t i = 0; i < num_output; i++) {
			oss << output[i] << " ";
		}
		oss <<  "\n";
		oss <<  "\n";
		std::string input = oss.str();
		writeFile("training.data", input);
	}

	void writeInputToVector() {
		std::ostringstream oss;
		for (size_t i = 0; i < num_input; i++) {
			oss << static_cast<float>(extractedFeatures[i]) << " ";
		}
		oss << "\n";
		std::array<int, 8> output = encodeToOneInC(iterations / 5000);
		for (size_t i = 0; i < num_output; i++) {
			oss << output[i] << " ";
		}
		oss << "\n";
		oss << "\n";
		std::string input = oss.str();
		inputVector.push_back(input);
	}

	void writeVectorToFile() {
		std::random_shuffle(inputVector.begin(), inputVector.end());
		for (auto str : inputVector) {
			writeFile("training.data", str);
		}
		
	}



	std::array<int, 8> encodeToOneInC(int type) {
		std::array<int, 8> encodedType = { 0,0,0,0,0,0,0,0 };
		if (type % 2 == 0)
			encodedType[0] = 1;
		else if (type == 1) {
			encodedType[type] = 1;
		}
		else {
			encodedType[(int)ceil((double)type/2.0)] = 1;
		}
		return encodedType;
	}

	bool fileExists(const char* file) {
		struct stat buf;
		return (stat(file, &buf) == 0);
	}

	void writeFile(std::string filename, std::string input) {
		using namespace std;
		ofstream myfile;
		myfile.open("c:\\1\\Speech Test 1\\reco\\"+filename, std::ios_base::app);
		myfile << input;
		myfile.close();
	}


    // onArmSync() is called whenever Myo has recognized a Sync Gesture after someone has put it on their
    // arm. This lets Myo know which arm it's on and which way it's facing.
    void onArmSync(myo::Myo* myo, uint64_t timestamp, myo::Arm arm, myo::XDirection xDirection, float rotation,
                   myo::WarmupState warmupState)
    {
        onArm = true;
        whichArm = arm;
    }

    // onArmUnsync() is called whenever Myo has detected that it was moved from a stable position on a person's arm after
    // it recognized the arm. Typically this happens when someone takes Myo off of their arm, but it can also happen
    // when Myo is moved around on the arm.
    void onArmUnsync(myo::Myo* myo, uint64_t timestamp)
    {
        onArm = false;
    }

    // onUnlock() is called whenever Myo has become unlocked, and will start delivering pose events.
    void onUnlock(myo::Myo* myo, uint64_t timestamp)
    {
        isUnlocked = true;
    }

    // onLock() is called whenever Myo has become locked. No pose events will be sent until the Myo is unlocked again.
    void onLock(myo::Myo* myo, uint64_t timestamp)
    {
        isUnlocked = false;
    }

};

int main(int argc, char** argv)
{
    // We catch any exceptions that might occur below -- see the catch statement for more details.
    try {

	//std::string command = "del /Q ";
	//std::string path = "\"c:\\1\\Speech Test 1\\reco\\*\"";
	//system(command.append(path).c_str());

	std::cout << "Waiting for myo to connect..." << std::endl;
	myo::Hub hub("com.example.hello-myo");
    myo::Myo* myo = hub.waitForMyo(10000);

    if (!myo) {
        throw std::runtime_error("Unable to find a Myo!");
    }
	   std::cout << "Connected to a Myo armband!" <<  "\n"; 
	myo->setStreamEmg(myo::Myo::streamEmgEnabled);
	hub.setLockingPolicy(myo::Hub::lockingPolicyNone);
    DataCollector collector;
    hub.addListener(&collector);

	std::cout << "Starting to read data, please follow the instruction" << std::endl;
    while (1) {
        hub.run(5);
    }


	////Test department...VS2015 sucks big time
	////encodeToOneInC
	//DataCollector collector;
	//for (int i = 0; i < 1; i++) {
	//	std::cout << i << " | ";
	//	for (int j : collector.encodeToOneInC(i)) {
	//		std::cout << j << " ";
	//	}
	//	std::cout << std::endl;
	//}
	////softmax
	//fann_type type[9] = {0.0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8};
	//std::cout << type << " | ";
	//for (auto j : collector.ceilAll(collector.softmax(type))) {
	//	std::cout << j << " "; 
	//}
	//std::cout << std::endl;
	//int i;
	//std::cin >> i;

    // If a standard exception occurred, we print out its message and exit.
    } catch (const std::exception& e) {
        std::cerr << "Error: " << e.what() <<  "\n";
        std::cerr << "Press enter to continue.";
        std::cin.ignore();
        return 1;
    }
}